//JSON Objects
//JSON stands for Javascript object Notation
//JSON is used for serializing different data types into bytes.
//serialization is the process of converting data into a series of bytes for easier of bytes for easier transmission/transfer of information
//byte =? binary digits (1 and 0) that is used to represent a character

/*
JSON data format
Syntax:
{
	"propertyA": "valueA",
	"propertyB": "value"
}


*/

/*
JS Object
{
	city: "QC",
	province: "Metro Manila",
	country: "Philippines"
}
*/

/*
JSON
{
	"city": "Metro Manila",
	"province": "Metro Manila",
	"country": "Philippines"
}
*/


/*
JSON Array
*/

/*"cities": [
	{

		"city": "Metro Manila",
		"province": "Metro Manila",
		"country": "Philippines"
	}

	{

		"city": "Metro Manila",
		"province": "Metro Manila",
		"country": "Philippines"
	}
	{

		"city": "Metro Manila",
		"province": "Metro Manila",
		"country": "Philippines"
	}
		]*/	

//JSON METHOD
//The JSON contains methods for parsing and converting data into stringified JSON

/*
-stringified JSON is a javascript object converted into a string to be used in other function of a Javascript application
*/


let batchesArr = [
	{
		batchName: "Batch X"
	},
	{
		batchName: "Batch X"
	}
]

	console.log("Result from stringify method")

//The stringify method is used to convert JS Objects into JSON(String)
console.log(JSON.stringify(batchesArr))

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
})

console.log(data);

/*Mini Activity*/

/*let user = JSON.stringify({
	firstName: prompt("Please enter first name:"),
	lastName: prompt("Please enter last name:"),
	age: Number(prompt("Please enter your age:")),
	adress: {
		city: prompt("Please enter city:"),
		country: prompt("Please enter city:"),
		zipCode: prompt("Please enter zip code:")
	}
})

console.log(user);
*/

//converting stringify JSON into JS objects
//Information is commonly sent to applications in stringified JSON and then converted back into objects.
//This happens both for sending Information back to frontend app

//upon receiving data, JSON text can be converted to a JS Object with parse

let batchesJSON = `[
	{
		"batchName": "Batch X"
	},
	{
		"batchName": "Batch X"
	}
]`
console.log("Result from parse method")
console.log(JSON.parse(batchesJSON))

let stringifiedObject = `[
	{
		"name": "John",
		"age": "31",
		"address": {
			"city": "Manila",
			"country": "Philippines"
		}
	}
]`

console.log(JSON.parse(stringifiedObject))
























































